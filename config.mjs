import * as dotenv from 'dotenv'; 
dotenv.config();

export const config = {
    MQTT_TOPIC: process.env.MQTT_TOPIC || 'zoom/status',
    MQTT_URL: process.env.MQTT_URL || 'mqtt://localhost',
    PROCESS_INTERVAL_MS: process.env.PROCESS_INTERVAL_MS || 5000,
    TARGET_PROCESS: process.env.TARGET_PROCESS || '~CptHost',
    ZOOM_LOGSTREAM: process.env.ZOOM_LOGSTREAM || false,
}