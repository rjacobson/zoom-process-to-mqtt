#Verify Mac
if [[ ! $(uname -s) == 'Darwin' ]]; then
    echo "This only works on Mac"
    exit 1
fi

# https://support.zoom.com/hc/en/article?id=zm_kb&sysparm_article=KB0060047
plutil -replace "enable\\.memlog\\.file" -string "true"  ~/Library/Preferences/ZoomChat.plist

dest="$HOME/Library/LaunchAgents"
cp me.robertjacobson.zoom-light.plist $dest 

fp=$dest/me.robertjacobson.zoom-light.plist

launchctl load $fp
launchctl enable $fp