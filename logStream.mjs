import { EventEmitter } from 'node:events';
import { config } from './config.mjs';
import { initMqttClient, publishStatusMessage } from './mqtt.mjs';
import { setupZoomWatcher, setupLogStream } from './zoom.mjs'

(async () => {
    console.log('----------------------------------------------------------------------------------------------')
    const emitter = new EventEmitter()
    
    initMqttClient()

    emitter.on('zoom', async status => {
        publishStatusMessage(status) 
    })
    if (config.ZOOM_LOGSTREAM) setupLogStream(emitter)
    setupZoomWatcher(emitter)
})()