import chalk from "chalk";

function begin(){
    const dts = new Date().toISOString()
    return `[${dts}]`
}

export function log(...args) {
    console.log(begin(), ...args)
}

export function info(...args){
    console.log(chalk.grey(begin(), ...args))
}