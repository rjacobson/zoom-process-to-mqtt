import * as mqtt from 'async-mqtt';
import {config} from './config.mjs'
import {log} from './logger.mjs';
import chalk from 'chalk';

let mqttClient = null

export function initMqttClient(){
    mqttClient = establishMqttConnection()

    mqttClient.on('error',      e => { log(chalk.red('Mqtt failed '), e)      })
    mqttClient.on('close',      () => { establishMqttConnection('closed')     })
    mqttClient.on('disconnect', () => { establishMqttConnection('disconnect') })
    mqttClient.on('connect',    () => { log(chalk.green('Mqtt connected successfully.')) })
}


function establishMqttConnection(prevState='') {
    if (mqttClient && mqttClient.reconnecting){
        log(chalk.yellow('Mqtt client reconnecting, skipping event'))
        return mqttClient
    }

    try{
        log(`Connecting to Mqtt server: ${chalk.underline(config.MQTT_URL)} ` +
            `topic: ${chalk.underline(config.MQTT_TOPIC)} ` + 
            `${prevState ? ' was: ' + chalk.underline(prevState) : ''}`
        )
        let mqttClient = mqtt.connect(config.MQTT_URL)

        return mqttClient
    }
    catch(e){
         console.error(chalk.bgRed('Failed to connect to MQTT server: '), e) 
    }
}
export async function publishStatusMessage(status){
    const msg = JSON.stringify({ state: status})
    await mqttClient.publish(config.MQTT_TOPIC, msg)
}