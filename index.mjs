import { EventEmitter } from 'node:events';
import { setupZoomWatcher } from './zoom.mjs';
import { initMqttClient, publishStatusMessage } from './mqtt.mjs';

(async () => {
    console.log('----------------------------------------------------------------------------------------------')
    const emitter = new EventEmitter();
    
    initMqttClient()

    emitter.on('zoom', async status => {
        publishStatusMessage(status) 
    })
    setupZoomWatcher(emitter)
})()