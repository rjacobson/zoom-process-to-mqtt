import psaux from 'psaux';
import { config } from './config.mjs';
import {info, log} from './logger.mjs';
import chalk from 'chalk';

let onCall = false

export function setupLogStream(emitter){
    const stdin = process.stdin
    stdin.setEncoding('utf-8')

    stdin.on('data', data => {
        try{
            const j = JSON.parse(data)
            if(j.eventMessage.includes(`"VDCAssistant_Power_State" = On`)){
                emitter.emit('zoom', 'Camera_On')
            }
            if(j.eventMessage.includes(`"VDCAssistant_Power_State" = Off`)){
                if (onCall) emitter.emit('zoom', toZoomStatus(onCall))
                else emitter.emit('zoom', 'Camera_Off')
            }
        } catch(e){}
    })

    stdin.on('end', () => {
        process.exit(1)
    });
}

export function setupZoomWatcher(emitter){
    info('Setting up Zoom check interval')
    return setInterval(() => checkProcess(emitter), config.PROCESS_INTERVAL_MS)
}

async function checkProcess(emitter){
    const procs = await psaux()
    const zoom = procs.query({command: config.TARGET_PROCESS})
    const callExists = zoom.length > 0
    if (callExists !== onCall){
        const newStatus = toZoomStatus(callExists)
        log(`Zoom state changed from ${chalk.underline(toZoomStatus(onCall))} to ${callExists ? chalk.green(newStatus) : chalk.red(newStatus)}`)
        onCall = callExists
        emitter.emit('zoom', newStatus)
    }
}

const toZoomStatus = (state) => state ? 'On_Phone_Call' : 'Available' 